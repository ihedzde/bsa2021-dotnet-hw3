﻿using System;
using CoolParking.ClientConsole.Interfaces;
using CoolParking.BL.Models;
using System.IO;
using CoolParking.ClientConsole.Helpers;
using CoolParking.ClientConsole.Functions;
using System.Threading.Tasks;
namespace CoolParking.ClientConsole
{
    class Program
    {
        static async Task ShowMenu(IParkingService parkingService){
            var menu = @$"Select one of following categories 1 - 9 or q to quit:{Environment.NewLine}
            1 - Current balance{Environment.NewLine}
            2 - Earned money{Environment.NewLine}
            3 - Available parking spots{Environment.NewLine}
            4 - Read current transactions{Environment.NewLine}
            5 - Read history of transactions{Environment.NewLine}
            6 - List current vehicles parked in parking{Environment.NewLine}
            7 - Park a vehicle{Environment.NewLine}
            8 - Checkout vehicle{Environment.NewLine}
            9 - Top up vehicle balance{Environment.NewLine}
            ";
            Console.WriteLine(menu);
            var choice = Console.ReadLine();
            while(choice!="q" && choice!="Q"){
                switch(choice){
                    case "1": {
                        await Function.CurrentBalance(parkingService);
                        break;
                    }
                    case "2":{
                        await Function.EarnedMoney(parkingService);
                        break;
                    }
                    case "3":{
                        await Function.AvailableParkingPlaces(parkingService);
                        break;
                    }
                    case "4":{
                        await Function.RecentTransactions(parkingService);
                        break;
                    }
                    case "5":{
                        await Function.ReadLogHistory(parkingService);
                        break;
                    }
                    case "6":{
                        await Function.CurrentParkedVehicles(parkingService);
                        break;
                    }
                    case "7":{
                        await Function.AddToParking(parkingService);
                        break;
                    }
                    case "8":{
                        await Function.RemoveFromParking(parkingService);
                        break;
                    }
                    case "9":{
                        await Function.TopUpVehicle(parkingService, Helper.GetLicensePlate());
                        break;
                    }
                    case string q when (q.ToLower() == "q"):{
                        return;
                    }
                    default:{
                        Console.WriteLine("No valid case was selected");
                        break;
                    }
                }
                Console.WriteLine(menu);
                choice = Console.ReadLine();
            }
        }


        static async Task Main(string[] args)
        {
            await ShowMenu(Helper.SetUp("http://localhost:5000/api"));
        }
    }
}
