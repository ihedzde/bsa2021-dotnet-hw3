using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Threading.Tasks;
using System.Text;
using CoolParking.ClientConsole.Models;
using CoolParking.ClientConsole.Interfaces;
using Newtonsoft.Json;

namespace CoolParking.ClientConsole
{
    public class CommunicationService : IParkingService
    {
        private readonly HttpClient _httpClient;
        public string BaseURL { get; set; }
        public CommunicationService(string baseURL)
        {
            _httpClient = new HttpClient();
            BaseURL = baseURL;
        }
        public async Task<decimal> GetBalance()
        {
            var responseBody = await _httpClient.GetStringAsync(BaseURL + "/parking/balance");
            return decimal.Parse(responseBody);
        }
        public async Task<int> GetCapacity()
        {
            var responseBody = await _httpClient.GetStringAsync(BaseURL + "/parking/capacity");
            return int.Parse(responseBody);
        }
        public async Task<int> GetFreePlaces()
        {
            var responseBody = await _httpClient.GetStringAsync(BaseURL + "/parking/freePlaces");
            return int.Parse(responseBody);
        }
        public async Task<string> GenerateRandomRegistrationPlateNumber()
        {
            var responseBody = await _httpClient.GetStringAsync(BaseURL + "/parking/genLicencePlate");
            return responseBody.Trim('"');
        }
        public async Task<ReadOnlyCollection<Vehicle>> GetVehicles()
        {
            var responseBody = await _httpClient.GetStringAsync(BaseURL + "/vehicles");
            return new ReadOnlyCollection<Vehicle>(JsonConvert.DeserializeObject<IList<Vehicle>>(responseBody.Trim('"').Replace(@"\", "")));
        }
        public async Task AddVehicle(Vehicle vehicle)
        {
            var json = JsonConvert.SerializeObject(vehicle);
            var response = await _httpClient.PostAsync(BaseURL + "/vehicles", new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.StatusCode == HttpStatusCode.BadRequest)
                throw new ArgumentException(await response.Content.ReadAsStringAsync());
            if (response.StatusCode == HttpStatusCode.PreconditionFailed)
                throw new InvalidOperationException(await response.Content.ReadAsStringAsync());

        }
        public async Task RemoveVehicle(string vehicleId)
        {
            var response = await _httpClient.DeleteAsync(BaseURL + $"/vehicles/{vehicleId}");
            if (response.StatusCode == HttpStatusCode.BadRequest)
                throw new ArgumentException(await response.Content.ReadAsStringAsync());
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new ArgumentException(await response.Content.ReadAsStringAsync());
            if (response.StatusCode == HttpStatusCode.PreconditionFailed)
                throw new InvalidOperationException(await response.Content.ReadAsStringAsync());
        }
        public async Task TopUpVehicle(string vehicleId, decimal sum)
        {
            var json = "{\"id\":\"" + vehicleId + "\",\"sum\":" + 1000.ToString() + "}";
            var response = await _httpClient.PutAsync(BaseURL + $"/transactions/topUpVehicle", new StringContent(json, Encoding.UTF8, "application/json"));
            if (response.StatusCode == HttpStatusCode.BadRequest)
                throw new ArgumentException(await response.Content.ReadAsStringAsync());
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new ArgumentException(await response.Content.ReadAsStringAsync());
            if (response.StatusCode == HttpStatusCode.PreconditionFailed)
                throw new InvalidOperationException(await response.Content.ReadAsStringAsync());
        }
        public async Task<TransactionInfo[]> GetLastParkingTransactions()
        {
            var responseBody = await _httpClient.GetStringAsync(BaseURL + "/transactions/last");
            return JsonConvert.DeserializeObject<TransactionInfo[]>(responseBody);
        }
        public async Task<string> ReadFromLog()
        {
            var response = await _httpClient.GetAsync(BaseURL + "/transactions/last");
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new InvalidOperationException(await response.Content.ReadAsStringAsync());
            var json = await response.Content.ReadAsStringAsync();
            return json;
        }
        public void Dispose()
        {
            _httpClient.Dispose();
        }

    }
}