using CoolParking.ClientConsole.Interfaces;
using System;
using System.Collections.ObjectModel;
using CoolParking.ClientConsole.Models;
using System.Threading.Tasks;
namespace CoolParking.ClientConsole.Interfaces
{
    public interface IParkingService:IDisposable
    {
        Task<decimal> GetBalance();
        Task<int> GetCapacity();
        Task<int> GetFreePlaces();
        Task<ReadOnlyCollection<Vehicle>> GetVehicles();
        Task AddVehicle(Vehicle vehicle);
        Task RemoveVehicle(string vehicleId);
        Task TopUpVehicle(string vehicleId, decimal sum);
        Task<TransactionInfo[]> GetLastParkingTransactions();
        Task<string> ReadFromLog();
        Task<string> GenerateRandomRegistrationPlateNumber();
    }
}