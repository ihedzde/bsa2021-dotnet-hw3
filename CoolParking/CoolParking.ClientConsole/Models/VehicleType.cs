﻿namespace CoolParking.ClientConsole.Models
{
    public enum VehicleType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}