using System;
namespace CoolParking.ClientConsole.Models
{
    public class Vehicle
    {
        #region Properties
        private readonly string id;
        private readonly VehicleType vehicleType;

        public string Id => id;
        public VehicleType VehicleType => vehicleType;
        public decimal Balance { get; internal set; }
        #endregion

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            this.id = id;
            this.vehicleType = vehicleType;

            Balance = balance;
        }
        public override string ToString()
        {
            string type = "";
            switch (VehicleType)
            {
                case VehicleType.Motorcycle: type = "motorcycle"; break;
                case VehicleType.Truck: type = "truck"; break;
                case VehicleType.Bus: type = "bus"; break;
                case VehicleType.PassengerCar: type = "passeger car"; break;
            }
            return $"Vehicle {Id} of type {type} has {Balance} on the balance";
        }
    }
}