﻿// TODO: [COOL-5] implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
namespace CoolParking.ClientConsole.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }
        public DateTime Time { get; set; }
        
        public override string ToString()
        {
            return $"{VehicleId} vehicle at {Time} charged {Sum}";
        }
    }
}