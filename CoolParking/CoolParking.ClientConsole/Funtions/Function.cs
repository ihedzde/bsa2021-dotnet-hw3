using System;
using CoolParking.ClientConsole.Interfaces;
using CoolParking.ClientConsole.Models;
using System.Linq;
using CoolParking.ClientConsole.Helpers;
using System.Threading.Tasks;
namespace CoolParking.ClientConsole.Functions
{
    public static class Function
    {
        public static async Task AddToParking(IParkingService service)
        {
            try
            {
                string plate;
                Console.WriteLine("Do you wish to generate new local licence plate?");
                if (Helper.YesOrNoQuestion())
                {
                    plate = await service.GenerateRandomRegistrationPlateNumber();
                }
                else
                {
                    plate = Helper.GetLicensePlate();
                }
                VehicleType type = Helper.SelectVehicleType();
                decimal sum = 0;
                Console.WriteLine("Do you wish to top up?");
                if (Helper.YesOrNoQuestion())
                {
                    Console.WriteLine($"Please input top up sum: ");
                    var sum_input = Console.ReadLine();
                    decimal? output;
                    output = Helper.ValidateSumInput(sum_input);
                    if (output == null)
                        return;
                    sum = output ?? 0;
                }
                Vehicle vehicle = new Vehicle(plate, type, sum);
                await service.AddVehicle(vehicle);
                Console.WriteLine($"{vehicle.ToString()} was added on the parking.");

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static async Task RemoveFromParking(IParkingService service)
        {
            var plate = Helper.GetLicensePlate();
            try
            {
                var vehicle = (await service.GetVehicles()).SingleOrDefault((v) => v.Id == plate);
                await service.RemoveVehicle(plate);
                Console.WriteLine($"{vehicle} was removed.");
                if (vehicle.Balance > 0)
                    Console.WriteLine($"Get your change {vehicle.Balance}");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static async Task CurrentParkedVehicles(IParkingService service)
        {
            var vehicles = await service.GetVehicles();
            if (vehicles.Count == 0)
            {
                Console.WriteLine("No vehicles are on the parking.");
            }
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{vehicle} is parked");
            }
        }
        public static async Task ReadLogHistory(IParkingService service)
        {
            try
            {
                Console.WriteLine(await service.ReadFromLog());
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public static async Task RecentTransactions(IParkingService service)
        {
            var list = await service.GetLastParkingTransactions();
            if (list.Length == 0)
            {
                Console.WriteLine("No recent transactions occured.");
                return;
            }
            foreach (var transactions in list)
            {
                Console.WriteLine(transactions.ToString());
            }
        }
        public static async Task EarnedMoney(IParkingService service)
        {
            decimal earned = 0;
            foreach (var transactions in await service.GetLastParkingTransactions())
            {
                earned += transactions.Sum;
            }
            Console.WriteLine($"Earned {earned} amount");
        }
        public static async Task CurrentBalance(IParkingService service)
        {
            Console.WriteLine($"Current balance {await service.GetBalance()}");
        }
        public static async Task AvailableParkingPlaces(IParkingService service)
        {
            Console.WriteLine($"There are {await service.GetFreePlaces()} available places from {await service.GetCapacity()} in the parking.");
        }
        public static async Task TopUpVehicle(IParkingService service, string licensePlate)
        {
            Console.WriteLine($"Please input top up sum: ");
            var sum_input = Console.ReadLine();
            try
            {
                decimal? output;
                output = Helper.ValidateSumInput(sum_input);
                if (output == null)
                    return;
                decimal sum = output ?? 0;
                await service.TopUpVehicle(licensePlate, sum);
                Console.WriteLine($"The vehicle with license plate {licensePlate} was topped up.");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}