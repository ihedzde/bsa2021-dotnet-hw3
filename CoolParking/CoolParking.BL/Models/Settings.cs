﻿// TODO: [COOL-4] implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
namespace CoolParking.BL.Models
{
    using System;
    using System.IO;
    using System.Reflection;

    public static class Settings
    {
        public static int ChargeRate { get; private set; } = 5;
        public static int LogRate { get; private set; } = 60;
        public static decimal Balance { get; private set; } = 0;
        public static decimal FineRate { get; private set; } = 2.5M;
        public static string LogFilePath { get; private set; } = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        public static int ParkingPlacesCount { get; private set; } = 10;
        public static decimal GetRate(VehicleType type)
        {
            switch (type)
            {
                case VehicleType.PassengerCar: return 2;
                case VehicleType.Bus: return 3.5M;
                case VehicleType.Truck: return 5;
                case VehicleType.Motorcycle: return 1;
                default: throw new ArgumentException($"No case for this type:{type}.");
            }
        }
    }
}