﻿// TODO: [COOL-2] implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
namespace CoolParking.BL.Models
{
    public enum VehicleType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}