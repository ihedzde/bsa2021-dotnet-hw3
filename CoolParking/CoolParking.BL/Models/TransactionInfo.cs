﻿// TODO: [COOL-5] implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
namespace CoolParking.BL.Models
{
    public readonly struct TransactionInfo
    {
        public string VehicleId { get; }
        public decimal Sum { get;  }
        public DateTime Time { get;}
        public TransactionInfo(string id, decimal sum, DateTime time){
            VehicleId = id;
            Sum = sum;
            Time = time;
        }
        
        public override string ToString()
        {
            return $"{VehicleId} vehicle at {Time} charged {Sum}";
        }
    }
}