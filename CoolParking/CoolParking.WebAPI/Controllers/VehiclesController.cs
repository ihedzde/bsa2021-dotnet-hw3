﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTOs;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> GetVehicles()
        {
            return Ok(_parkingService.GetVehicles());
        }
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetVehicleById(string id)
        {
            if (!Vehicle.ValidatePlate(id))
                return BadRequest("Id is invalid");
            var vehicle = _parkingService.GetVehicles().SingleOrDefault<Vehicle>((v) => v.Id == id);
            if (vehicle == null)
            {
                return NotFound("Vehicle not found.");
            }
            return Ok(vehicle);
        }
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status412PreconditionFailed)]
        public IActionResult AddVehicle([FromBody] VehiclePOSTDTO vehicleDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid body.");
            }
            Vehicle vehicle;
            try
            {
                 vehicle = new Vehicle(vehicleDTO.Id, vehicleDTO.VehicleType, vehicleDTO.Balance);
                _parkingService.AddVehicle(vehicle);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(412, e.Message);
            }
            return CreatedAtAction(nameof(AddVehicle), vehicle);
        }
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status412PreconditionFailed)]
        public IActionResult RemoveVehicle(string id)
        {
            if (!Vehicle.ValidatePlate(id))
                return BadRequest("Invalid plate");
            try
            {
                _parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(412, e.Message);
            }
            return NoContent();
        }
    }
}
