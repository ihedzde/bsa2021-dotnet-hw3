﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTOs;


namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public ActionResult<IEnumerable<TransactionInfo>> GetLastTransactions()
        {
            return Ok((IEnumerable<TransactionInfo>)_parkingService.GetLastParkingTransactions());
        }
        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<string> GetLogsTransaction()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpPut("topUpVehicle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult TopUpVehicle([FromBody] VehiclePUTDTO vehicleDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest("Body is invalid.");
            if(!Vehicle.ValidatePlate(vehicleDTO.Id))
                return BadRequest("Id(license plate) is invalid.");
            if (vehicleDTO.Sum < 0)
                return BadRequest("Negative top up sum.");
            var vehicle = _parkingService.GetVehicles().SingleOrDefault<Vehicle>((v) => v.Id == vehicleDTO.Id);
            if (vehicle == null)
            {
                return NotFound($"No vehicle on parking with {vehicleDTO.Id}.");
            }
            try
            {
                _parkingService.TopUpVehicle(vehicleDTO.Id, vehicleDTO.Sum);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            return Ok(vehicle);
        }
    }
}
