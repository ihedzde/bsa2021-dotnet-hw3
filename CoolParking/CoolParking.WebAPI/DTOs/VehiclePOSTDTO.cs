using System.ComponentModel.DataAnnotations;
using CoolParking.BL.Models;
namespace CoolParking.WebAPI.DTOs{

    public class VehiclePOSTDTO{
        [Required]
        public string Id {get; set;}
        [Required]
        [Range(0, 3)]
        public VehicleType VehicleType {get; set;}
        [Required]
        [RegularExpression(@"^\d+\.\d{0,2}$", ErrorMessage="Balance isn't valid decimal.")]
        [Range(0, 9999999999999999.99)]
        public decimal Balance{get; set;}
    }
}