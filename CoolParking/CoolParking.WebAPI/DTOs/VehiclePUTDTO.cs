using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.DTOs{

    public class VehiclePUTDTO{
        [Required]
        public string Id {get; set;}
        [Required]
        [RegularExpression(@"^\d+\.\d{0,2}$", ErrorMessage="Sum isn't valid decimal value.")]
        [Range(0.01, 9999999999999999.99)]
        public decimal Sum{get; set;}
    }
}